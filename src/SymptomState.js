"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SymptomState;
(function (SymptomState) {
    SymptomState[SymptomState["NO"] = 0] = "NO";
    SymptomState[SymptomState["MILD"] = 1] = "MILD";
    SymptomState[SymptomState["HIGH"] = 2] = "HIGH";
})(SymptomState = exports.SymptomState || (exports.SymptomState = {}));
